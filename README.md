The gem index and alignability data for hg19 genome is alreay avaialble. This
repo contains the 'gem' index and alignability data for GRCh38 version of human 
genome, as it takes time to generate the data.

All the data has beem generated using the following guide:

    https://wiki.bits.vib.be/index.php/Create_a_mappability_track
    
Following 'GEM' library was used to generate the data:

    https://sourceforge.net/projects/gemlibrary/files/gem-library/Binary%20pre-release%202/
    
To reproduce the data, please follow the steps below:

1) Reference genome --> GRCh38.p12
2) Index generation:
    gemtools index -i GRCh38.p12.fasta -t 40

3) Compute Alignability for several K-mer lengths
    gem-mappability -I GRCh38.p12.gem -l 50 -o GRCh38_mappability_50mer.gem
    gem-mappability -I GRCh38.p12.gem -l 100 -o GRCh38_mappability_100mer.gem
    gem-mappability -I GRCh38.p12.gem -l 150 -o GRCh38_mappability_150mer.gem